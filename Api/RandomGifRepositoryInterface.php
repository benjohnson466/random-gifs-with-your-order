<?php

namespace BenJ\RandomGif\Api;

use BenJ\RandomGif\Api\Data\RandomGifInterface;
use Magento\Framework\Exception\NoSuchEntityException;

interface RandomGifRepositoryInterface
{
    /**
     * @param RandomGifInterface $randomGif
     * @return RandomGifInterface
     */
    public function save(RandomGifInterface $randomGif);

    /**
     * @param $randomGifId
     * @return RandomGifInterface
     * @throws NoSuchEntityException
     */
    public function getById($randomGifId);

    /**
     * @param RandomGifInterface $randomGif
     * @return void
     */
    public function delete(RandomGifInterface $randomGif);

    /**
     * @param $randomGifId
     * @return void
     * @throws NoSuchEntityException
     */
    public function deleteById($randomGifId);

    /**
     * @param $orderEntityId
     * @return RandomGifInterface
     * @throws NoSuchEntityException
     */
    public function getByOrderEntityId($orderEntityId);
}