<?php

namespace BenJ\RandomGif\Api\Data;

interface RandomGifInterface
{
    const RANDOM_GIF_ID = 'random_gif_id';
    const SALES_ORDER_ENTITY_ID = 'sales_order_entity_id';
    const RANDOM_GIF_URL = 'random_gif_url';
    const CREATED_AT = 'created_at';

    public function getId();

    public function getSalesOrderEntityId();

    public function getRandomGifUrl();

    public function getCreatedAt();

    public function setId($id);

    public function setSalesOrderEntityId($entityId);

    public function setRandomGifUrl($randomGifUrl);

    public function setCreatedAt($timestamp);
}