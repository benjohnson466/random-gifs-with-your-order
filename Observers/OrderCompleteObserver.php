<?php

namespace BenJ\RandomGif\Observers;

use Magento\Framework\Event\Observer;
use Magento\Sales\Model\Order\Interceptor as OrderInterceptor;

class OrderCompleteObserver extends AbstractOrderObserver
{
    /**
     * Get a gif URL and save to the db
     *
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var OrderInterceptor $order */
        $order = $observer->getOrder();

        // get a gif model
        $randomGif = $this->randomGifService->getRandomGif($order);

        $this->logger->debug('Random gif fetch complete', [
            'url' => $randomGif->getRandomGifUrl(),
            'order_id' => $order->getId(),
            'random_gif_id' => $randomGif->getId(),
        ]);
    }
}