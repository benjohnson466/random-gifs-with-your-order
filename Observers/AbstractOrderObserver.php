<?php

namespace BenJ\RandomGif\Observers;

use BenJ\RandomGif\RandomGifService;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractOrderObserver implements ObserverInterface
{
    /**
     * @var RandomGifService
     */
    protected $randomGifService;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * AbstractOrderObserver constructor
     *
     * @param RandomGifService $randomGifService
     * @param LoggerInterface $logger
     */
    public function __construct(RandomGifService $randomGifService, LoggerInterface $logger)
    {
        $this->randomGifService = $randomGifService;
        $this->logger = $logger;
    }
}