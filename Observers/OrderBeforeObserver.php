<?php

namespace BenJ\RandomGif\Observers;

use Magento\Framework\Event\Observer;

class OrderBeforeObserver extends AbstractOrderObserver
{
    /**
     * Send request to get a gif
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $this->randomGifService->fetch();
    }
}