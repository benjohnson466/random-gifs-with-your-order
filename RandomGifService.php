<?php

namespace BenJ\RandomGif;

use BenJ\RandomGif\Model\RandomGif;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\PromiseInterface;
use BenJ\RandomGif\Model\RandomGifFactory;
use BenJ\RandomGif\Model\RandomGifRepository;
use Magento\Sales\Model\Order\Interceptor as OrderInterceptor;

class RandomGifService
{
    /**
     * Giphy's random gif endpoint
     */
    const GIPHY_API_ENDPOINT = 'https://api.giphy.com/v1/gifs/random?api_key=%s&rating=G';

    /**
     * Guzzle client
     *
     * @var Client
     */
    private $client;

    /**
     * Request promise
     *
     * @var PromiseInterface
     */
    private $promise;

    /**
     * @var RandomGifFactory
     */
    private $randomGifFactory;

    /**
     * @var RandomGifRepository
     */
    private $randomGifRepository;

    /**
     * RandomGifService constructor
     *
     * @param Client $client
     * @param RandomGifFactory $randomGifFactory
     * @param RandomGifRepository $randomGifRepository
     */
    public function __construct(
        Client $client,
        RandomGifFactory $randomGifFactory,
        RandomGifRepository $randomGifRepository
    ) {
        $this->client = $client;
        $this->randomGifFactory = $randomGifFactory;
        $this->randomGifRepository = $randomGifRepository;
    }

    /**
     * Send a request to Giphy for a random gif
     *
     * @return void
     */
    public function fetch()
    {
        // TODO: dont hardcode api key, obvs
        $this->promise = $this->client->getAsync(sprintf(self::GIPHY_API_ENDPOINT, '5fff767082ba4acaa75e686961724fec'));
    }

    /**
     * Process the response (if received) and generate a random gif
     *
     * @param OrderInterceptor $orderInterceptor
     * @return RandomGif|null
     */
    public function getRandomGif(OrderInterceptor $orderInterceptor)
    {
        if ($response = $this->wait()) {
            $giphyData = json_decode($response->getBody()->getContents());
            // did we get an image url?
            if ($randomGifUrl = ($giphyData->data->image_url ?? null)) {
                // create a random gif model instance
                /** @var RandomGif $randomGif */
                $randomGif = $this->randomGifFactory->create();
                $randomGif->setRandomGifUrl($randomGifUrl);
                $randomGif->setSalesOrderEntityId($orderInterceptor->getEntityId());
                $this->randomGifRepository->save($randomGif);
                return $randomGif;
            }
        }
        return null;
    }

    /**
     * Wait for promise to be fulfilled and get response
     *
     * @return string|null
     */
    private function wait()
    {
        $response = $this->promise->wait();
        if ($response->getStatusCode() === 200) {
            return $response;
        }
        return null;
    }
}