<?php

namespace BenJ\RandomGif\Test\User;

use BenJ\RandomGif\Api\Data\RandomGifInterface;
use BenJ\RandomGif\Observers\OrderBeforeObserver;
use BenJ\RandomGif\Observers\OrderCompleteObserver;
use BenJ\RandomGif\RandomGifService;
use Magento\Framework\Event\Observer;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class OrderObserverTest extends TestCase
{
    private $randomGifServiceMock;

    private $loggerMock;

    private $interceptorMock;

    private $orderId;

    private $gifUrl;

    protected function setUp()
    {
        $this->orderId = mt_rand(1, 10000);
        $this->gifUrl = 'https://media2.giphy.com/media/xTkcESOZqaaQWSmS76/giphy.gif';

        // get a mocked order interceptor
        $this->interceptorMock = $this->getMockBuilder(\Magento\Sales\Model\Order\Interceptor::class)
            ->disableOriginalConstructor()
            ->getMock();

        // mock the random gif service
        $this->randomGifServiceMock = $this->getMockBuilder(RandomGifService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->loggerMock = $this->getMockBuilder(LoggerInterface::class)->getMock();
    }

    /**
     * Test OrderBeforeObserver
     */
    public function testOrderBeforeObserver()
    {
        // gif service expectations for this class
        $this->randomGifServiceMock->expects($this->once())->method('fetch');

        $eventObserverMock = $this->getMockBuilder(Observer::class)->getMock();

        $orderBeforeObserver = new OrderBeforeObserver($this->randomGifServiceMock, $this->loggerMock);
        $orderBeforeObserver->execute($eventObserverMock);
    }

    /**
     * Test OrderCompleteObserver
     *
     * @throws \Exception
     */
    public function testOrderCompleteObserver()
    {
        // interceptor expectations
        $this->interceptorMock->expects($this->once())
            ->method('getId')
            ->willReturn($this->orderId);

        // mock a random gif model
        $randomGifId = mt_rand(1, 1000);
        $randomGifMock = $this->getMockBuilder(RandomGifInterface::class)->getMock();
        $randomGifMock->expects($this->once())
            ->method('getRandomGifUrl')
            ->willReturn($this->gifUrl);
        $randomGifMock->expects($this->once())
            ->method('getId')
            ->willReturn($randomGifId);

        // gif service expectations
        $this->randomGifServiceMock->expects($this->once())
            ->method('getRandomGif')
            ->with($this->interceptorMock)
            ->willReturn($randomGifMock);

        // event observer should be called via magic method
        $eventObserverMock = $this->getMockBuilder(Observer::class)->getMock();
        $eventObserverMock->expects($this->once())
            ->method('__call')
            ->with('getOrder')
            ->willReturn($this->interceptorMock);

        // log expectations
        $this->loggerMock->expects($this->once())
            ->method('debug')
            ->with('Random gif fetch complete', [
                'url' => $this->gifUrl,
                'order_id' => $this->orderId,
                'random_gif_id' => $randomGifId,
            ]);

        $orderCompleteObserver = new OrderCompleteObserver($this->randomGifServiceMock, $this->loggerMock);
        $orderCompleteObserver->execute($eventObserverMock);
    }
}