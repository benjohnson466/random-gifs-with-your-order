<?php

namespace BenJ\RandomGif\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $tableName = $setup->getTable('benj_randomgif_randomgifs');

        if (!$setup->getConnection()->isTableExists($tableName)) {
            $setup->getConnection()
                ->createTable(
                    $setup->getConnection()
                        ->newTable($tableName)
                        ->addColumn('random_gif_id', Table::TYPE_INTEGER, null, [
                            'identity' => true,
                            'unsigned' => true,
                            'nullable' => false,
                            'primary' => true,
                        ], 'ID')
                        ->addColumn('sales_order_entity_id', Table::TYPE_INTEGER, null, [
                            'unsigned' => true,
                            'nullable' => false,
                        ], 'Order ID')
                        ->addColumn('random_gif_url', Table::TYPE_TEXT, null, [
                            'nullable' => false,
                        ], 'Giphy URLolz')
                        ->addColumn('created_at', Table::TYPE_TIMESTAMP, null, [
                            'nullable' => false,
                            'default' => Table::TIMESTAMP_INIT,
                        ], 'Created at')
                        ->setComment('Y tho?')
                );
        }

        $setup->endSetup();
    }
}