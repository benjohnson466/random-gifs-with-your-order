<?php

namespace BenJ\RandomGif\Model\ResourceModel\RandomGif;

use BenJ\RandomGif\Model\RandomGif;
use BenJ\RandomGif\Model\ResourceModel\RandomGif as RandomGifResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        return $this->_init(RandomGif::class, RandomGifResourceModel::class);
    }
}