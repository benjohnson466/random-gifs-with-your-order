<?php

namespace BenJ\RandomGif\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class RandomGif extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('benj_randomgif_randomgifs', 'random_gif_id');
    }
}