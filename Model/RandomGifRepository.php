<?php

namespace BenJ\RandomGif\Model;

use BenJ\RandomGif\Api\Data\RandomGifInterface;
use BenJ\RandomGif\Api\RandomGifRepositoryInterface;
use BenJ\RandomGif\Model\ResourceModel\RandomGif as RandomGifResource;
use BenJ\RandomGif\Model\ResourceModel\RandomGif\Collection as RandomGifCollection;

class RandomGifRepository implements RandomGifRepositoryInterface
{
    /**
     * @var RandomGifResource
     */
    private $randomGifResource;

    /**
     * @var RandomGifFactory
     */
    private $randomGifFactory;

    /**
     * @var RandomGifCollection
     */
    private $randomGifCollection;

    /**
     * RandomGifRepository constructor
     *
     * @param RandomGifResource $randomGifResource
     * @param RandomGifFactory $randomGifFactory
     * @param RandomGifCollection $collection
     */
    public function __construct(
        RandomGifResource $randomGifResource,
        RandomGifFactory $randomGifFactory,
        RandomGifCollection $collection
    ) {
        $this->randomGifResource = $randomGifResource;
        $this->randomGifFactory = $randomGifFactory;
        $this->randomGifCollection = $collection;
    }

    /**
     * @inheritdoc
     */
    public function save(RandomGifInterface $randomGif)
    {
        $this->randomGifResource->save($randomGif);
    }

    /**
     * @inheritdoc
     */
    public function getById($randomGifId)
    {
        $this->randomGifResource->load(
            $randomGif = $this->randomGifFactory->create(),
            $randomGifId
        );
        return $randomGif;
    }

    /**
     * @inheritdoc
     */
    public function delete(RandomGifInterface $randomGif)
    {
        $this->randomGifResource->delete($randomGif);
    }

    /**
     * @inheritdoc
     */
    public function deleteById($randomGifId)
    {
        $this->delete($this->getById($randomGifId));
    }

    /**
     * @inheritdoc
     */
    public function getByOrderEntityId($orderEntityId)
    {
        $this->randomGifResource->load(
            $randomGif = $this->randomGifFactory->create(),
            $orderEntityId,
            RandomGifInterface::SALES_ORDER_ENTITY_ID
        );
        return $randomGif;
    }
}
