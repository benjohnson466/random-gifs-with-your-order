<?php

namespace BenJ\RandomGif\Model;

use DateTime;
use BenJ\RandomGif\Api\Data\RandomGifInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use BenJ\RandomGif\Model\ResourceModel\RandomGif as RandomGifResourseModel;

class RandomGif extends AbstractModel implements RandomGifInterface, IdentityInterface
{
    const CACHE_TAG = 'benj_randomgif_randomgif';

    protected function _construct()
    {
        $this->_init(RandomGifResourseModel::class);
    }

    public function getIdentities()
    {
        return [sprintf('%s_%s', self::CACHE_TAG, $this->getId())];
    }

    public function getSalesOrderEntityId()
    {
        return $this->getData(self::SALES_ORDER_ENTITY_ID);
    }

    public function getRandomGifUrl()
    {
        return $this->getData(self::RANDOM_GIF_URL);
    }

    public function getCreatedAt()
    {
        return DateTime::createFromFormat('Y-m-d H:i:s', $this->getData(self::CREATED_AT));
    }

    public function setSalesOrderEntityId($entityId)
    {
        return $this->setData(self::SALES_ORDER_ENTITY_ID, $entityId);
    }

    public function setRandomGifUrl($randomGifUrl)
    {
        return $this->setData(self::RANDOM_GIF_URL, $randomGifUrl);
    }

    public function setCreatedAt($timestamp)
    {
        return $this->setData(self::CREATED_AT, $timestamp);
    }

}