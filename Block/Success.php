<?php

namespace BenJ\RandomGif\Block;

use BenJ\RandomGif\Api\Data\RandomGifInterface;
use BenJ\RandomGif\Model\RandomGifRepository;
use Magento\Checkout\Block\Onepage\Success as CheckoutSuccess;

class Success extends CheckoutSuccess
{
    /**
     * @var RandomGifRepository
     */
    private $randomGifRepository;

    /**
     * Success constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param RandomGifRepository $randomGifRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Framework\App\Http\Context $httpContext,
        RandomGifRepository $randomGifRepository,
        array $data = []
    ) {
        parent::__construct($context, $checkoutSession, $orderConfig, $httpContext, $data);

        $this->randomGifRepository = $randomGifRepository;
    }

    /**
     * Get the random gif associated with this order (or not)
     *
     * @return RandomGifInterface|null
     */
    public function getRandomGif()
    {
        return $this->randomGifRepository
            ->getByOrderEntityId($this->_checkoutSession->getLastRealOrder()->getEntityId());
    }
}